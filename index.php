<!DOCTYPE html>
<html lang="en">
<head>
  <title>Prosa.ai Demo Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="public/css/bootstrap.min.css">
  <script src="public/js/jquery.js"></script>
  <script src="public/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center" style="background: #347a68">
  <img src="public/images/header.png" alt="shape-5" style="position: fixed; top: -380px; left: -80px; z-index:2">
  <div style="position: relative; z-index: 10; top:2rem; color:white">
    <h1>Prosa.ai Demo Page</h1>
    <p>for Indonesia Association of Computational Linguistics</p>
  </div>
</div>
  
<div class="container">
  <div class="row">
    <div class="col-6">
      <div class="form-group">
        <label for="input-text">Input Text</label>
        <textarea type="text" class="form-control" id="input-text" name="text" onkeyup="changetext()" placeholder="Input text to be analyzed" style="height:10rem"></textarea>
      </div>
      <div class="form-group">
        <label class="radio-inline"><input type="radio" name="choosen-api" value="word-normalization" checked>Word Normalization</label><br/>
        <label class="radio-inline"><input type="radio" name="choosen-api"  value="sentiment-analysis" >Sentiment Analysis</label><br/>
        <label class="radio-inline"><input type="radio" name="choosen-api"  value="hatespeech" >Hate Speech Detector</label><br/>
      </div>
      <button class="btn btn-primary" onclick="analisis()">Analyze...</button>
    </div>
    <div class="col-6">
      <div id="normalized-word-container">
        Normalized text:
        <p id="normalized-word"></p>
      </div>
      <div id="sentiment-container" style="display: none">
        Sentiment:
        <br/><span id="sentiment" style="color:blue; font-weight: bold"></span><span id="conf-level"></span>
      </div>
      <div id="hatespeech-container" style="display: none">
        Result:
        <br/><span id="hatespeech" style="color:blue; font-weight: bold"></span><span id="conf-level-hate"></span>
      </div>
    </div>
  </div>
</div>

</body>
</html>

<script type="text/javascript">
  var api="word-normalization";
  var url='https://api.prosa.ai/v1/normals';
  var data;
  $('input:radio[name="choosen-api"]').change(
    function(){
        if (this.checked) {
            if ($(this).val() == "word-normalization") {
              api = "word-normalization";
              url = 'https://api.prosa.ai/v1/normals';
            }
            else if ($(this).val() == "sentiment-analysis") {
              api = "sentiment-analysis";
              url = 'https://api.prosa.ai/v1/sentiments';
            }
            else if ($(this).val() == "hatespeech") {
              api = "hatespeech";
              url = 'https://api.prosa.ai/v1/hates';
            }
        }
    });

  function changetext() {
    data = $('#input-text').val();
  }
  function analisis(){
    $.ajax({
          url: url,
          type: 'post',
          data: JSON.stringify({text: data}),
          headers: {
              'Content-Type' : 'application/json',
              'x-api-key' : 'RngwLraT5g7soNN8KcseJPD5FXnPo9rX6g3vypwI'
          },
          dataType: 'json',
          success: function (data) {
              if (api == "word-normalization") {
                $("#normalized-word").html(data.normalized_text);
                $("#normalized-word-container").show();
                $("#sentiment-container").hide();
                $("#hatespeech-container").hide();
              }
              else if (api == "sentiment-analysis") {
                $("#sentiment").html(data.polarity);
                if (data.polarity == "negative") {
                  $("#sentiment").css("color", "red");
                }
                else if (data.polarity == "positive") {
                  $("#sentiment").css("color", "green");
                }
                else {
                  $("#sentiment").css("color", "blue");
                }
                $("#conf-level").html(" with confidence level of "+data.confidence);
                $("#normalized-word-container").hide();
                $("#sentiment-container").show();
                $("#hatespeech-container").hide();
              }
              else if (api == "hatespeech") {
                  if (data.hate_speech == true) {
                    $("#hatespeech").html("Hate speech");
                    $("#hatespeech").css("color", "red");
                  }
                  else {
                    $("#hatespeech").html("Not hate speech");
                    $("#hatespeech").css("color", "green");
                  }
                  $("#conf-level-hate").html(" with confidence level of "+data.confidence);
                  $("#normalized-word-container").hide();
                  $("#sentiment-container").hide();
                  $("#hatespeech-container").show();
              }
          },
          error: function (data) {
            console.log(data);
          }
      });
    }

</script>